﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RkCore.Helper.Enums;

namespace RkCore.Model
{
    public class AlertModel
    {
        public AlertModel(AlertStatus alertStatus, string alertMessage)
        {
            this.alertStatus = alertStatus;
            this.alertMessage = alertMessage;
        }
        public AlertStatus alertStatus { get; set; }
        public string alertMessage { get; set; }
    }

    public class UploadImageModel
    {
        public string imageUrl { get; set; }
        public string saveFilePath { get; set; }
        public string hdnImageName { get; set; }
    }
    public class SaveImageModel
    {
        public string imgBase64String { get; set; }
        public string saveFilePath { get; set; }
        public string extension { get; set; }
        public string previousImageName { get; set; }
    }
}
