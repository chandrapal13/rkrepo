﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace RkCore.Model
{
    public class ViewContactUsModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string Message { get; set; }

        public DateTime CreateDate { get; set; }
    }

    public class ContactUsModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage ="Please Enter Name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please Enter Phone Number")]
        [Phone(ErrorMessage ="Please Enter Proper Number")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage ="Please Enter Email Address")]
        [EmailAddress(ErrorMessage = "Please Enter Valid Email Address")]
        public string Email { get; set; }
        [Required(ErrorMessage ="Please Enter Messege")]
        public string Message { get; set; }

        
    }
}
