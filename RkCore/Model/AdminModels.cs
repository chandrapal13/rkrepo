﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RkCore.Helper.Enums;

namespace RkCore.Model
{
    public class AdminLoginModel
    {
        public AdminLoginModel()
        {
            Role = UserRoles.Admin;
            RememberMe = false;
        }

        [Required]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string EmailId { get; set; }

        [Required]
        [DisplayName("Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }


        public UserRoles Role { get; set; }

        public bool RememberMe { get; set; }

    }

    public class AdminChangePasswordModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string CurrentPassword { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string ConfirmPassword { get; set; }
    }

    public class ForgotPasswordModel
    {
        [Required]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string Email { get; set; }
    }

    public class ResetPasswordModel
    {

        public string UserId { get; set; }

        [Display(Name = "password reset token")]
        [Required(ErrorMessage = "{0} is required")]
        public string PasswordResetToken { get; set; }

        [Display(Name = "email address")]
        [Required(ErrorMessage = "Please enter {0}")]
        [EmailAddress(ErrorMessage = "Please enter valid {0}")]
        public string EmailID { get; set; }

        [Display(Name = "new password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Display(Name = "confirm password")]
        [Required(ErrorMessage = "Please enter {0}")]
        [DataType(DataType.Password)]
        [Compare("NewPassword", ErrorMessage = "Comfirm Password does not match with Password")]
        public string ConfirmPassword { get; set; }

    }


}
