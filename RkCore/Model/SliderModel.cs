﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkCore.Model
{
    public class AddUpdateSliderModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please enter headertext")]
        public string HeaderText { get; set; }

        [Required(ErrorMessage ="Please enter desccrption")]
        public string Description { get; set; }

        [Required(ErrorMessage ="Please upload Image")]
        public string sliderimage { get; set; }
    }
}
