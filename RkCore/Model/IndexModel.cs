﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkCore.Model
{
    public class IndexModel
    {
        public List<AddUpdateSliderModel> Slider { get; set; }

        public List<ProductListModel> ProductList { get; set; }
    }
}
