﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkCore.Model
{
    public class AdminDashboardModel
    {
        public int TotalInquiry { get; set; }

        public int TotalProducts { get; set; }
        
    }
}
