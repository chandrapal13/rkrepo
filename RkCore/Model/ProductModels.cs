﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkCore.Model
{

    public class ProductListModel
    {
        public int ProductId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public string VideoLink { get; set; }

        public List<ProductImagesModel> ProductImages { get; set; }
        public List<ProductModelName> ProductModels { get; set; }

    }

    #region  Admin Panel

    public class ViewProductModel
    {
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string VideoLink { get; set; }
        public List<ProductPropertyModel> ProductProperty { get; set; }
        public List<ProductImagesModel> ProductImages { get; set; }
        public List<ProductFeatureModel> Producteatures { get; set; }
        public List<ProductBenfitsModel> ProductBenfts { get; set; }
        public List<ProductListModel> ProductList { get; set; }
    }

    public class ProductPropertyModel
    {
        public int ProductId { get; set; }
        public string PropertyName { get; set; }
        public string PropertyValue { get; set; }
    }

    public class ProductImagesModel
    {
        public int ProductId { get; set; }
        public string ImageName { get; set; }
    }

    public class ProductFeatureModel
    {
        public int ProductId { get; set; }

        public string Feature { get; set; }
    }

    public class ProductBenfitsModel
    {
        public int ProductId { get; set; }

        public string Benifit { get; set; }
    }

    public class ProductModelName
    {
        public int ProductId { get; set; }

        public string ModelName { get; set; }
    }

    public class ProductBaseModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Product Name Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Model Number Required")]
        public string ModelName { get; set; }

        [Required(ErrorMessage = "Model Number Required")]
        public string Description { get; set; }
    }

    public class AddProductModeltems
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Model Number Required")]
        public string Name { get; set; }

        public string VideoLink { get; set; }

        [Required(ErrorMessage = "Model Number Required")]
        public string Description { get; set; }

        public string HiddenModelImages { get; set; }

        public string DefaultImage { get; set; }

        public string[] ModelImages { get; set; }

        public List<ProductPropertyModel> ProductProperty { get; set; }
        public List<ProductFeatureModel> ProductFeatures { get; set; }
        public List<ProductBenfitsModel> ProductBenefits { get; set; }
        public List<ProductModelName> ProductModels { get; set; }

    }
    #endregion
}
