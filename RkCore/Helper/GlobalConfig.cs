﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkCore.Helper
{
    public class GlobalConfig
    {
        public static string baseUrl = ConfigurationManager.AppSettings["BaseRouteUrl"].ToString();
        public static string baseFileUrl = ConfigurationManager.AppSettings["BaseRouteFileUrl"].ToString();
        public static string baseRoutePath = ConfigurationManager.AppSettings["BaseRoutePath"];


        //public static bool isLocalServer = bool.Parse(ConfigurationManager.AppSettings["isLocalServer"]);

        public const string ProjectName = "RkManufactures.in";
        public const string defaultImageName = "default.png";

        public const int PageSize_AdminPanel = 10;
        public const string BreakRow = "<BR/>";

        public const string dateFormat = "MMMM dd, yyyy";

        public static string fileRoutePath = baseRoutePath + @"Files\";
        public static string fileBaseUrl = baseFileUrl + "Files/";

        public static string ProductImagePath = fileRoutePath + @"ProductImages\";
        public static string ProductImageUrl = fileBaseUrl + "ProductImages/";

        public static string SliderImagePath = fileRoutePath + @"SliderImages\";
        public static string SliderImageUrl = fileBaseUrl + "SliderImages/";

        public static string CatalogImagePath = fileRoutePath + @"Catalog\";
        public static string CatalogImageUrl = fileBaseUrl + "Catalog/";

        public const int PageSize = 10;
        public const int NotificationPageSize = 1000;

        #region Email credentials

        public const string infoEmailAddress = "no-reply@imperoit.com";
        public const string EmailUserName = "no-reply@imperoit.com";
        public const string emailPassword = "Noorani@123";
        public const string emailSMTPClient = "smtp.zoho.com";
        public const string developerEmail = "sadik.impero@gmail.com";

        #endregion Email credentials
    }
}
