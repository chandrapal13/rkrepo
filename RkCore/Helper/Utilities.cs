﻿using RkCore.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RkCore.Helper.Enums;

namespace RkCore.Helper
{
    public static class Utilities
    {
        public static DateTime GetSystemDateTime()
        {
            return DateTime.Now;
        }
        public static DateTime GetSystemDateTimeUTC()
        {
            return DateTime.UtcNow;
        }

        public static ResponseModel<object> SaveCropImage(string imgBase64String, string saveFilePath, string extension, int height, int width)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                imgBase64String = imgBase64String.Replace("data:image/png;base64,", "");

                byte[] bytes = Convert.FromBase64String(imgBase64String);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    ms.Position = 0;
                    image = Image.FromStream(ms);
                }
                ImageOptimize.FixedSize(saveFilePath, image, width, height, true);
                //if (extension.ToLower() != "png")
                //    ImageOptimize.FixedSize(saveFilePath, image, width, height, true);
                //else
                //{
                //    image.Save(saveFilePath, ImageFormat.Png);
                //}
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Image uploaded ";
            }
            catch (Exception err)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = err.Message;
                mResult.Result = err;
            }

            return mResult;
        }
    }
}
