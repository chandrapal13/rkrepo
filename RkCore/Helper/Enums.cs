﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkCore.Helper
{
    public class Enums
    {
        public enum AlertStatus
        {
            success,
            info,
            warning,
            error,
        }
               

        public enum ResponseStatus
        {
            /// <summary>
            /// request failed due to some reason.
            /// </summary>
            Failed = 0,
            /// <summary>
            /// system signout the app or website forcefully.
            /// </summary>
            ForceSignout = 100,
            /// <summary>
            /// request succeeded and that the requested information is in the response.
            /// </summary>
            Success = 200,
            /// <summary>
            /// that the request has been successfully processed and that the response is intentionally blank.
            /// </summary>
            NoContent = 204,
            /// <summary>
            /// that the requested resource requires authentication.
            /// </summary>
            Unauthorized = 401,
            /// <summary>
            /// that the client should switch to a different protocol such as TLS/1.0.
            /// </summary>
            UpgradeRequired = 426,
            /// <summary>
            /// indicates that a generic error has occurred on the server.
            /// </summary>
            InternalServerError = 500,
            /// <summary>
            /// that the server does not support the requested function.
            /// </summary>
            NotImplemented = 501,
            /// <summary>
            /// the requested resource does not exist on the server.
            /// </summary>
            NotFound = 404,
            /// <summary>
            ///  that the client did not send a request within the time the server was expecting the request.
            /// </summary>
            RequestTimeout = 408,
            /// <summary>
            /// that the server is temporarily unavailable, usually due to high load or maintenance.
            /// </summary>
            ServiceUnavailable = 503,
        }

        public enum UserRoles
        {
            Admin,
            User                      
        }

        public enum Gender
        {
            Male,
            Female,
            Other
        }
    }
}
