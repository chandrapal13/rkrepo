﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace RK
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.AppendTrailingSlash = true;
            routes.LowercaseUrls = true;
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute
            (name: "pdetail",
            url: "product/{name}",
            defaults: new { controller = "Product", action = "ProductDetail", name = typeof(string) }
           );

            routes.MapRoute
            (name: "products",
            url: "products.html",
            defaults: new { controller = "Product", action = "Index", Id = "0" }
           );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = "0" }
            );




        }
    }
}
