﻿using System.Web;
using System.Web.Optimization;

namespace RK
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content/css/bootstrap.min.css",
                     "~/Content/css/material-design-iconic-font.min.css",                     
                     "~/Content/css/plugins.css",
                     "~/Content/style.css"));


            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
               "~/Content/js/vendor/jquery-1.12.0.min.js"
             ));

            bundles.Add(new ScriptBundle("~/js/contentjs").Include(
             "~/Content/js/bootstrap.min.js",
              "~/Content/js/plugins.js",              
              "~/Content/js/ajax-mail.js",
              "~/Content/js/main.js",
              "~/Content/js/vendor/modernizr-2.8.3.min.js"));

            bundles.Add(new ScriptBundle("~/jQuery/validate").Include(
                    "~/Content/Plugins/Validate/jquery.validate.min.js",
                    "~/Content/Plugins/Validate/jquery.validate.unobtrusive.min.js",
                    "~/Content/Plugins/Validate/jquery.unobtrusive-ajax.min.js"
                    ));

        }
    }
}
