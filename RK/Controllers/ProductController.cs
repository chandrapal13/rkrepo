﻿using RkCore.Model;
using RkServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace RK.Controllers
{
    public class ProductController : Controller
    {
        private ProductServices _product;
        public ProductController()
        {
            _product = new ProductServices();
        }
        // GET: Product
        public async Task<ActionResult> Index()
        {
            List<ProductListModel> model = await _product.GetProductList();
            return View(model);
        }
      
        public async Task<ActionResult> ProductDetail(string name)
        {
            ViewProductModel model = await _product.ProductDetailById(name);
            model.ProductList = await _product.GetProductList();
            return View(model);
        }
    }
}