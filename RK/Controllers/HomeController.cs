﻿using RkCore.Model;
using RkServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static RkCore.Helper.Enums;

namespace RK.Controllers
{
    public class HomeController : Controller
    {
        private GeneralServices _general;
        private ProductServices _product;
        public HomeController()
        {
            _general = new GeneralServices();
            _product = new ProductServices();
        }
        public async Task<ActionResult> Index()
        {
            IndexModel model = new IndexModel();
            model.Slider = await _general.getSlider();
            model.ProductList =await _product.GetProductList();
            
            return View(model);
        }

        public ActionResult About()
        {        
            return View();
        }
       
        public ActionResult ContactUs()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Contact(ContactUsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _general.AddContact(model);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.error, mResult.Message);
            return RedirectToAction("ContactUs");
        }

        public ActionResult Gallary()
        {
            return View();
        }

        public ActionResult WhyUs()
        {
            return View();
        }
    }
}