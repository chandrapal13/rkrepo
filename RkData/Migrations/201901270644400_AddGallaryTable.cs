namespace RkData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddGallaryTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Gallary",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImageName = c.String(),
                        Title = c.String(),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Gallary");
        }
    }
}
