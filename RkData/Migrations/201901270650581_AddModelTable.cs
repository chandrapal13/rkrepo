namespace RkData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddModelTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductModel",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ModelName = c.String(),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Products", t => t.ProductId, cascadeDelete: true)
                .Index(t => t.ProductId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductModel", "ProductId", "dbo.Products");
            DropIndex("dbo.ProductModel", new[] { "ProductId" });
            DropTable("dbo.ProductModel");
        }
    }
}
