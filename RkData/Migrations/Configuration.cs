namespace RkData.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using RkData.Entities;
    using RkData.Identity;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using static RkCore.Helper.Enums;

    internal sealed class Configuration : DbMigrationsConfiguration<RkData.Identity.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            var userManager = new ApplicationUserManager(new ApplicationStore(context));
            var roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context));
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            #region Insert Role If Not Exist
            string roleName = UserRoles.User.ToString();
            var role = roleManager.FindByNameAsync(roleName).Result;
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleResult = roleManager.CreateAsync(role).Result;
            }

            //roleName = UserRoles.HR.ToString();
            //role = roleManager.FindByNameAsync(roleName).Result;
            //if (role == null)
            //{
            //    role = new IdentityRole(roleName);
            //    var roleresult = roleManager.CreateAsync(role).Result;
            //}

            roleName = UserRoles.Admin.ToString();
            role = roleManager.FindByNameAsync(roleName).Result;
            if (role == null)
            {
                role = new IdentityRole(roleName);
                var roleresult = roleManager.CreateAsync(role).Result;
            }
            #endregion
            
            #region Add Admin User
            const string emailId = "admin@rkmanufactures.com";
            const string userName = "Admin";
            const string password = "Admin@123";
            var user = userManager.FindByNameAsync(userName).Result;
            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = userName,
                    Email = emailId,
                    Name = "Admin",
                    GenderId = Gender.Male,
                    Role = UserRoles.Admin         
                };
                try
                {
                    var result = userManager.CreateAsync(user, password).Result;
                    result = userManager.SetLockoutEnabledAsync(user.Id, false).Result;
                }
                catch (Exception ex)
                {

                    throw ex;
                }

            }
            #endregion

            #region Add Admin Role to Admin User
            var rolesForUser = userManager.GetRolesAsync(user.Id).Result;
            if (!rolesForUser.Contains(UserRoles.Admin.ToString()))
            {
                var result = userManager.AddToRoleAsync(user.Id, UserRoles.Admin.ToString()).Result;
            }
            #endregion
        }
    }
}
