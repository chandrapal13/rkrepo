namespace RkData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addvideolink_in_Product : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Products", "videolink", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Products", "videolink");
        }
    }
}
