﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using RkCore.Helper;
using RkCore.Model;
using RkData.Entities;
using RkData.Identity;
using RkData.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using static RkCore.Helper.Enums;

namespace RkData.Repository
{
    public class UserRepository: IDisposable
    {
        private readonly ApplicationDbContext _ctx;
        private readonly ApplicationUserManager _userManager;
        private readonly IAuthenticationManager _authenticationManager;
        private readonly IOwinContext _owin;

        public UserRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx));
        }

        public UserRepository(IOwinContext owin)
        {
            _owin = owin;
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx))
            {
                EmailService = new Services.EmailService()
            };
            _authenticationManager = owin.Authentication;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser user = await _userManager.FindByEmailAsync(model.EmailId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid email address.";
            }
            else
            {
                bool validRole = await _userManager.IsInRoleAsync(user.Id, user.Role.ToString());

                if (validRole)
                {
                    bool validPassword = await _userManager.CheckPasswordAsync(user, model.Password);
                    if (validPassword)
                    {
                        await SignInAsync(user, false, false);

                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = string.Format("User {0} Logged in successfully!", user.UserName);
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Invalid password.";
                    }
                }
            }
            return mResult;
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent, bool rememberBrowser)
        {
            WebLogout();
            ClaimsIdentity userIdentity = await _userManager.CreateIdentityAsync(user,DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("FullName", user.UserName));
            _authenticationManager.SignIn(
                new AuthenticationProperties { IsPersistent = isPersistent },
                userIdentity);
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                ApplicationUser user = await _userManager.FindByIdAsync(model.UserId);
                if (user == null)
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Invalid username or password.";
                }
                else
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.CurrentPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Password changed successfully";
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Invalid password.";
                    }
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (DbEntityValidationResult eve in e.EntityValidationErrors)
                {
                    foreach (DbValidationError ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return mResult;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ResetPassword(ResetPasswordModel model, bool isSendEmail = true)
        {
            ResponseModel<object> mResult = new ResponseModel<object>()
            {
                Status = ResponseStatus.Failed
            };
            ApplicationUser user = await _userManager.FindByEmailAsync(model.EmailID);

            if (user != null)
            {
                IdentityResult result = await _userManager.ResetPasswordAsync(user.Id, model.PasswordResetToken, model.NewPassword);
                _userManager.UpdateSecurityStamp(user.Id);
                if (result.Succeeded)
                {
                    if (isSendEmail)
                    {
                        StringBuilder mailContent = new StringBuilder();
                        mailContent.Append("Hello " + user.Name + ",");
                        mailContent.Append("<BR/><BR/>");
                        mailContent.Append("Greetings from " + GlobalConfig.ProjectName + " as per your request, we have successfully changed your password.");
                        mailContent.Append("<BR/><BR/><BR/>");
                        mailContent.Append("Thanks");
                        mailContent.Append("<BR/>");
                        mailContent.Append(GlobalConfig.ProjectName + " Team!");

                        IdentityMessage message = new IdentityMessage
                        {
                            Subject = string.Format("{0}: Password reset", GlobalConfig.ProjectName),
                            Body = mailContent.ToString(),
                            Destination = user.Email
                        };
                        await new EmailService().SendAsync(message);
                        await _userManager.SendEmailAsync(user.Id, message.Subject, message.Body);
                    }

                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "password reset successfully!";
                }
                else
                {
                    mResult.Message = string.Join("; ", result.Errors);
                }
            }
            return mResult;
        }

        public void WebLogout()
        {
            _authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
               DefaultAuthenticationTypes.TwoFactorCookie);
        }


        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
