﻿using RkCore.Helper;
using RkCore.Model;
using RkData.Entities;
using RkData.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RkCore.Helper.Enums;

namespace RkData.Repository
{
    public class ProductRepo
    {
        private ApplicationDbContext _ctx;
        public ProductRepo()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<List<ProductListModel>> GetProductList()
        {
            return await (from P in _ctx.Product
                          select new ProductListModel
                          {
                              ProductId = P.Id,
                              Name = P.Name,
                              Description = P.Description,
                              VideoLink=P.videolink,
                              ProductImages = P.ProductImages.Select(x => new ProductImagesModel
                              {
                                  ProductId = x.ProductId,
                                  ImageName = x.ProductImage
                              }).ToList(),
                              ProductModels = P.ProductModel.Select(x => new ProductModelName
                              {
                                  ProductId = x.ProductId,
                                  ModelName = x.ModelName
                              }).ToList()
                          }).OrderBy(x => x.ProductId).ToListAsync();
        }

        public IQueryable<ViewProductModel> GetProductBaseList()
        {
            return (from p in _ctx.Product
                    select new ViewProductModel
                    {
                        ProductId = p.Id,
                        Name = p.Name,
                        Description = p.Description,
                        ProductProperty = p.ProductProperty.Select(x => new ProductPropertyModel
                        {
                            ProductId = x.ProductId,
                            PropertyName = x.PropertyName,
                            PropertyValue = x.PropertyValue
                        }).ToList(),
                        ProductImages = p.ProductImages.Select(x => new ProductImagesModel
                        {
                            ProductId = x.Id,
                            ImageName = x.ProductImage
                        }).ToList()
                    }).OrderBy(x => x.Name);
        }

        public async Task<ResponseModel<object>> AddEditProduct(AddProductModeltems model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Products ObjProduct = await _ctx.Product.FirstOrDefaultAsync(x => x.Id == model.Id);
                bool isNew = false;
                if (ObjProduct == null)
                {
                    ObjProduct = new Products();
                    isNew = true;
                };

                ObjProduct.Name = model.Name;
                ObjProduct.Description = model.Description;
                ObjProduct.videolink = model.VideoLink;

                if (!isNew)
                {
                    _ctx.ProductImages.RemoveRange(ObjProduct.ProductImages);
                    await _ctx.SaveChangesAsync();
                }

                if (model.ModelImages != null)
                {
                    ObjProduct.ProductImages = (from fl in model.ModelImages
                                                select new ProductImages
                                                {
                                                    ProductId = ObjProduct.Id,
                                                    ProductImage = fl
                                                }).ToList();
                }

                if (isNew)
                {
                    List<ProductProperty> propertylist = new List<ProductProperty>();
                    foreach (ProductPropertyModel item in model.ProductProperty)
                    {
                        if (!string.IsNullOrWhiteSpace(item.PropertyName))
                        {
                            propertylist.Add(new ProductProperty
                            {
                                PropertyName = item.PropertyName,
                                PropertyValue = item.PropertyValue,
                                ProductId = ObjProduct.Id
                            });
                        }
                    }
                    ObjProduct.ProductProperty = propertylist;

                    List<ProductFeatures> featurelist = new List<ProductFeatures>();
                    foreach (ProductFeatureModel item in model.ProductFeatures)
                    {
                        if (!string.IsNullOrWhiteSpace(item.Feature))
                        {
                            featurelist.Add(new ProductFeatures
                            {
                                Feature = item.Feature,
                                ProductId = ObjProduct.Id
                            });
                        }
                    }
                    ObjProduct.ProductFeatures = featurelist;

                    List<ProductBenfits> benifitlist = new List<ProductBenfits>();
                    foreach (ProductBenfitsModel item in model.ProductBenefits)
                    {
                        if (!string.IsNullOrWhiteSpace(item.Benifit))
                        {
                            benifitlist.Add(new ProductBenfits
                            {
                                Benifit = item.Benifit,
                                ProductId = ObjProduct.Id
                            });
                        }
                    }
                    ObjProduct.ProductBenfits = benifitlist;

                    List<ProductModel> ModelList = new List<ProductModel>();
                    foreach (ProductModelName item in model.ProductModels)
                    {
                        if (!string.IsNullOrWhiteSpace(item.ModelName))
                        {
                            ModelList.Add(new ProductModel
                            {
                                ModelName = item.ModelName,
                                ProductId = ObjProduct.Id
                            });
                        }
                    }
                    ObjProduct.ProductModel = ModelList;


                }
                else
                {
                    _ctx.ProductProperty.RemoveRange(_ctx.ProductProperty.Where(c => c.ProductId == model.Id));
                    await _ctx.SaveChangesAsync();
                    List<ProductProperty> Properties = new List<ProductProperty>();
                    foreach (ProductPropertyModel item in model.ProductProperty)
                    {
                        if (!string.IsNullOrWhiteSpace(item.PropertyName))
                        {
                            Properties.Add(new ProductProperty
                            {
                                PropertyName = item.PropertyName,
                                PropertyValue = item.PropertyValue,
                                ProductId = ObjProduct.Id
                            });
                        }
                    }
                    ObjProduct.ProductProperty = Properties;


                    _ctx.ProductFeatures.RemoveRange(_ctx.ProductFeatures.Where(x => x.ProductId == model.Id));
                    await _ctx.SaveChangesAsync();
                    List<ProductFeatures> featurelist = new List<ProductFeatures>();
                    foreach (ProductFeatureModel item in model.ProductFeatures)
                    {
                        if (!string.IsNullOrWhiteSpace(item.Feature))
                        {
                            featurelist.Add(new ProductFeatures
                            {
                                Feature = item.Feature,
                                ProductId = ObjProduct.Id
                            });
                        }
                    }
                    ObjProduct.ProductFeatures = featurelist;


                    _ctx.ProductBenfits.RemoveRange(_ctx.ProductBenfits.Where(x => x.ProductId == model.Id));
                    await _ctx.SaveChangesAsync();
                    List<ProductBenfits> benifitlist = new List<ProductBenfits>();
                    foreach (ProductBenfitsModel item in model.ProductBenefits)
                    {
                        if (!string.IsNullOrWhiteSpace(item.Benifit))
                        {
                            benifitlist.Add(new ProductBenfits
                            {
                                Benifit = item.Benifit,
                                ProductId = ObjProduct.Id
                            });
                        }
                    }
                    ObjProduct.ProductBenfits = benifitlist;


                    _ctx.ProductModel.RemoveRange(_ctx.ProductModel.Where(x => x.ProductId == model.Id));
                    await _ctx.SaveChangesAsync();
                    List<ProductModel> modelList = new List<ProductModel>();
                    foreach (ProductModelName item in model.ProductModels)
                    {
                        if (!string.IsNullOrWhiteSpace(item.ModelName))
                        {
                            modelList.Add(new ProductModel
                            {
                                ModelName = item.ModelName,
                                ProductId = ObjProduct.Id
                            });
                        }
                    }
                    ObjProduct.ProductModel = modelList;

                }

                if (isNew)
                {
                    _ctx.Product.Add(ObjProduct);
                }

                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    if (isNew)
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Product Added successfully";
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Product Edited successfully";
                    }
                }
            }

            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<AddProductModeltems> GetProductDetail(int ProductId)
        {
            AddProductModeltems result = await (from p in _ctx.Product
                                                where p.Id == ProductId
                                                select new AddProductModeltems
                                                {
                                                    Id = p.Id,
                                                    Name = p.Name,
                                                    Description = p.Description,
                                                    VideoLink=p.videolink,
                                                    ProductProperty = p.ProductProperty.Select(x => new ProductPropertyModel
                                                    {
                                                        ProductId = x.ProductId,
                                                        PropertyName = x.PropertyName,
                                                        PropertyValue = x.PropertyValue
                                                    }).ToList(),
                                                    ProductBenefits = p.ProductBenfits.Select(x => new ProductBenfitsModel
                                                    {
                                                        ProductId = x.ProductId,
                                                        Benifit = x.Benifit
                                                    }).ToList(),
                                                    ProductFeatures = p.ProductFeatures.Select(x => new ProductFeatureModel
                                                    {
                                                        ProductId = x.Id,
                                                        Feature = x.Feature
                                                    }).ToList(),

                                                    ProductModels = p.ProductModel.Select(x => new ProductModelName
                                                    {
                                                        ProductId = x.Id,
                                                        ModelName = x.ModelName
                                                    }).ToList()
                                                }).FirstOrDefaultAsync();
            if (result != null)
            {
                result.ModelImages = _ctx.ProductImages.Where(x => x.ProductId == ProductId).Select(x => x.ProductImage).ToArray();
                result.HiddenModelImages = String.Join(",", _ctx.ProductImages.Where(x => x.ProductId == ProductId).Select(x => x.ProductImage).ToArray());
            }
            return result;

        }

        public async Task<ResponseModel<object>> DeleteProduct(int id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Products objpro = _ctx.Product.FirstOrDefault(x => x.Id == id);
                if (objpro == null)
                {
                    mResult.Message = "Product not found";
                    mResult.Status = ResponseStatus.Failed;
                }
                else
                {
                    var existImages = _ctx.ProductImages.Where(x => x.ProductId == id).ToList();
                    foreach (var item in existImages)
                    {
                        string FilePath = GlobalConfig.ProductImagePath + item.ProductImage;
                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }
                    }

                    _ctx.ProductImages.RemoveRange(existImages);
                    _ctx.ProductProperty.RemoveRange(_ctx.ProductProperty.Where(x => x.ProductId == id).ToList());
                    _ctx.ProductBenfits.RemoveRange(_ctx.ProductBenfits.Where(x => x.ProductId == id).ToList());
                    _ctx.ProductFeatures.RemoveRange(_ctx.ProductFeatures.Where(x => x.ProductId == id).ToList());

                    _ctx.Product.Remove(objpro);
                }
                long objId = await _ctx.SaveChangesAsync();
                if (objId > 0)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Product Deleted successfully!";
                }
                else
                {
                    mResult.Result = new object { };
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Failed to delete Product!";
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteFileName(string filename)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ProductImages objpro = await _ctx.ProductImages.SingleOrDefaultAsync(x => x.ProductImage == filename);
                if (objpro == null)
                {
                    mResult.Message = "filename not found";
                    mResult.Status = ResponseStatus.Failed;
                }
                else
                {
                    var existImages = _ctx.ProductImages.Where(x => x.ProductImage == filename).ToList();
                    foreach (var item in existImages)
                    {
                        string FilePath = GlobalConfig.ProductImagePath + item.ProductImage;
                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }
                    }

                    _ctx.ProductImages.RemoveRange(existImages);

                    _ctx.ProductImages.Remove(objpro);
                }
                long objId = await _ctx.SaveChangesAsync();
                if (objId > 0)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "file Deleted successfully!";
                }
                else
                {
                    mResult.Result = new object { };
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Failed to delete file!";
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ViewProductModel> ProductDetailById(string name)
        {
            name = name.Replace("-", " ");
            ViewProductModel result = await (from p in _ctx.Product
                                             where p.Name.Equals(name)
                                             select new ViewProductModel
                                             {
                                                 ProductId = p.Id,
                                                 Name = p.Name,
                                                 Description = p.Description,
                                                 VideoLink=p.videolink,
                                                 ProductImages = p.ProductImages.Select(x => new ProductImagesModel
                                                 {
                                                     ProductId = x.ProductId,
                                                     ImageName = x.ProductImage
                                                 }).ToList(),
                                                 ProductProperty = p.ProductProperty.Select(x => new ProductPropertyModel
                                                 {
                                                     ProductId = x.ProductId,
                                                     PropertyName = x.PropertyName,
                                                     PropertyValue = x.PropertyValue
                                                 }).ToList(),
                                                 ProductBenfts = p.ProductBenfits.Select(x => new ProductBenfitsModel
                                                 {
                                                     ProductId = x.ProductId,
                                                     Benifit = x.Benifit
                                                 }).ToList(),
                                                 Producteatures = p.ProductFeatures.Select(x => new ProductFeatureModel
                                                 {
                                                     ProductId = x.Id,
                                                     Feature = x.Feature
                                                 }).ToList()
                                             }).FirstOrDefaultAsync();
            return result;
        }
    }
}
