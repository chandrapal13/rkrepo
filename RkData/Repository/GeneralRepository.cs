﻿using RkCore.Helper;
using RkCore.Model;
using RkData.Entities;
using RkData.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RkCore.Helper.Enums;

namespace RkData.Repository
{
    public class GeneralRepository
    {
        private ApplicationDbContext _ctx;
        public GeneralRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public AdminDashboardModel GetDeshboardInfo()
        {
            AdminDashboardModel model = new AdminDashboardModel();
            model.TotalInquiry = _ctx.ContactUs.Count();
            model.TotalProducts = _ctx.Product.Count();
            return model;
        }

        public async Task<List<AddUpdateSliderModel>> getSlider()
        {
            return await (from sld in _ctx.SLider
                          select new AddUpdateSliderModel
                          {
                              Id = sld.Id,
                              HeaderText = sld.HeaderText,
                              Description = sld.Description,
                              sliderimage = sld.ImageName
                          }).ToListAsync();
        }

        public async Task<ResponseModel<object>> AddOrUpdateSlider(AddUpdateSliderModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool isnew = false;
            Slider objslider = await _ctx.SLider.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (objslider == null)
            {
                isnew = true;
                objslider = new Slider();
            }

            objslider.HeaderText = model.HeaderText;
            objslider.Description = model.Description;
            objslider.ImageName = model.sliderimage;

            if (isnew)
            {
                _ctx.SLider.Add(objslider);
            }

            int id = await _ctx.SaveChangesAsync();
            if (id > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Slider added successfully";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "failed to add slider.";
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteSlider(int Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            Slider objsld = await _ctx.SLider.FirstOrDefaultAsync(x => x.Id == Id);
            if (objsld != null)
            {
                string images = _ctx.SLider.FirstOrDefault(x => x.Id == Id).ImageName;
                if (!string.IsNullOrEmpty(images))
                {
                    var filepath = GlobalConfig.SliderImagePath + images;
                    if (File.Exists(filepath))
                    {
                        File.Delete(filepath);
                    }
                }

                _ctx.SLider.Remove(objsld);
                await _ctx.SaveChangesAsync();

                mResult.Message = "Slider deleted successfully!";
                mResult.Status = ResponseStatus.Success;


            }
            else
            {
                mResult.Message = "data not found";
                mResult.Status = ResponseStatus.Failed;
            }

            return mResult;
        }

        #region
        //ContactUs Module

        public async Task<List<ViewContactUsModel>> GetContactUs()
        {
            return await (from cn in _ctx.ContactUs
                          select new ViewContactUsModel
                          {
                              Id = cn.Id,
                              Name = cn.Name,
                              Email = cn.Email,
                              PhoneNumber = cn.MobileNo,
                              Message = cn.Message
                          }).ToListAsync();
        }
        public async Task<ResponseModel<object>> AddContact(ContactUsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ContactUs con = new ContactUs();
                con.Name = model.Name;
                con.MobileNo = model.PhoneNumber;
                con.Email = model.Email;
                con.Message = model.Message;
                _ctx.ContactUs.Add(con);

                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    mResult.Message = "Thank you! your Message is successfully submited.";
                    mResult.Status = ResponseStatus.Success;

                }
                else
                {
                    mResult.Message = "failed to submit message";
                    mResult.Status = ResponseStatus.Failed;


                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;

            }

            return mResult;
        }
        #endregion
    }
}
