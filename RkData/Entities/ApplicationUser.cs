﻿using Microsoft.AspNet.Identity.EntityFramework;
using RkCore.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RkCore.Helper.Enums;

namespace RkData.Entities
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string Name { get; set; }

        [EnumDataType(typeof(Gender))]
        public Gender GenderId { get; set; } = Gender.Male;

        public string ProfileImageName { get; set; }
        
        public DateTime CreatedDateTimeUTC { get; set; } =Utilities.GetSystemDateTimeUTC();

        [EnumDataType(typeof(UserRoles))]
        public UserRoles Role { get; set; }
        
        public bool IsActive { get; set; } = true;

    }
}
