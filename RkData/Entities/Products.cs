﻿using RkData.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkData.Entities
{
    public class Products : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string videolink { get; set; }

        public virtual ICollection<ProductProperty> ProductProperty { get; set; }
        public virtual ICollection<ProductImages> ProductImages { get; set; }
        public virtual ICollection<ProductFeatures> ProductFeatures { get; set; }
        public virtual ICollection<ProductBenfits> ProductBenfits { get; set; }
        public virtual ICollection<ProductModel> ProductModel { get; set; }
    }

    public class ProductProperty : BaseIdEntity
    {
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Products Product { get; set; }

        public string PropertyName { get; set; }

        public string PropertyValue { get; set; }

    }

    public class ProductImages : BaseIdEntity
    {
        public string ProductImage { get; set; }

        public bool IsDefault { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Products Product { get; set; }

    }

    public class ProductFeatures : BaseIdEntity
    {
        public string Feature { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Products Product { get; set; }
    }

    public class ProductBenfits : BaseIdEntity
    {
        public string Benifit { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Products Product { get; set; }
    }

    public class ProductModel:BaseIdEntity
    {
        public string ModelName { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Products Product { get; set; }

    }
}
