﻿using RkCore.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkData.Entities.Base
{
    public class BaseEntity : BaseIdEntity
    {
        public DateTime CreatedDate { get; set; } = Utilities.GetSystemDateTime();
        public DateTime? UpdatedDate { get; set; }
    }
}
