﻿using RkData.Entities.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkData.Entities
{
    public class Gallary:BaseEntity
    {
        public string ImageName { get; set; }

        public string Title { get; set; }
    }
}
