﻿using Microsoft.Owin;
using RkCore.Model;
using RkData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkServices
{
    public class UserService
    {
        private UserRepository _repo;
        private readonly IOwinContext _owin;

        public UserService()
        {
            _repo = new UserRepository();
        }

        public UserService(IOwinContext owin)
        {
            _repo = new UserRepository(owin);
            this._owin = owin;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            return await _repo.AdminLogin(model);
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            return await _repo.AdminChangePassword(model);
        }
        public async Task<ResponseModel<object>> ResetPassword(ResetPasswordModel model)
        {
            return await _repo.ResetPassword(model);
        }

        public void WebLogout()
        {
            _repo.WebLogout();
        }
    }
}
