﻿using RkCore.Model;
using RkData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkServices
{
    public class GeneralServices
    {
        private GeneralRepository _repo;
        public GeneralServices()
        {
            _repo = new GeneralRepository();
        }

        public AdminDashboardModel GetDeshboardInfo()
        {
            return _repo.GetDeshboardInfo();
        }

        public async Task<List<AddUpdateSliderModel>> getSlider()
        {
            return await _repo.getSlider();
        }

        public async Task<ResponseModel<object>> AddOrUpdateSlider(AddUpdateSliderModel model)
        {
            return await _repo.AddOrUpdateSlider(model);
        }

        public async Task<ResponseModel<object>> DeleteSlider(int Id)
        {
            return await _repo.DeleteSlider(Id);
        }

        public async Task<List<ViewContactUsModel>> GetContactUs()
        {
            return await _repo.GetContactUs();
        }

        public async Task<ResponseModel<object>> AddContact(ContactUsModel model)
        {
            return await _repo.AddContact(model);
        }
    }
}
