﻿using RkCore.Model;
using RkData.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RkServices
{
    public class ProductServices
    {
        private ProductRepo _repo;
        public ProductServices()
        {
            _repo = new ProductRepo();
        }

        public async Task<List<ProductListModel>> GetProductList()
        {
            return await _repo.GetProductList();
        }

        public IQueryable<ViewProductModel> GetProductBaseList()
        {
            return _repo.GetProductBaseList();
        }

        //public async Task<ResponseModel<object>> AddOrEditProduct(ProductBaseModel model)
        //{
        //    return await _repo.AddOrEditProduct(model);
        //}

        //public async Task<ProductBaseModel> GetProductDetail(int id)
        //{
        //    return await _repo.GetProductDetail(id);
        //}

        public async Task<ResponseModel<object>> AddEditProduct(AddProductModeltems model)
        {
            return await _repo.AddEditProduct(model);
        }

        public async Task<AddProductModeltems> GetProductDetail(int ProductId)
        {
            return await _repo.GetProductDetail(ProductId);
        }

        public async Task<ResponseModel<object>> DeleteProduct(int id)
        {
            return await _repo.DeleteProduct(id);
        }
        public async Task<ResponseModel<object>> DeleteFileName(string filename)
        {
            return await _repo.DeleteFileName(filename);
        }
        public async Task<ViewProductModel> ProductDetailById(string name)
        {
            return await _repo.ProductDetailById(name);
        }
    }
}
