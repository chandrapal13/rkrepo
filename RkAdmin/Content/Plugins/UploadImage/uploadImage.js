﻿
function reset_html(hdnimg) {

    $("#fileImage_" + hdnimg).val('');

    $("#Modal_" + hdnimg).modal('hide');
}

function SaveCropedImage(saveFilePath, hdnImageName, postUrl) {

    //alert("Hidden field name" + hdnImageName); 
    StartBlockPage();
    var $image = $("#cropimage_" + hdnImageName);
    var result = $image.cropper("getCroppedCanvas");
    var imgData = result.toDataURL('image/png');


    var extension = $("#fileImage_" + hdnImageName).val().split('.').pop();
    console.log(extension);
    var prevImageName = $("#prev_" + hdnImageName).val();
    //console.log(prevImageName);

    var model = {
        imgBase64String: imgData,
        saveFilePath: saveFilePath,
        extension: extension,
        previousImageName: prevImageName
    };

    $.ajax({
        type: 'POST',
        url: postUrl,
        data: JSON.stringify(model),
        contentType: 'application/json; charset=utf-8',
        dataType: 'JSON',
        success: function (resultResponse) {
            console.log(resultResponse);
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(resultResponse.Message);
            if (resultResponse.Status == 200) {
                console.log("1");
                $("#solid-alerts .alert").addClass("bg-primary");
                //$('#ImageName').val(resultResponse.Result);
                $('#' + hdnImageName).val(resultResponse.Result);

                var div = '<div class="col-lg-3 col-md-6 col-xs-12">';
                div += '<img class="img-thumbnail img-fluid img-custom-size" data-new="1" data-imagename="' + resultResponse.Result + '" src="' + imgData + '"/><br/><br/>';
                div += '</div>';


                $('#divUpload').append(div);
                $('#spanErrMultiUpload').hide();
                // removeImage();
            }
            else {
                $("#solid-alerts .alert").addClass("bg-warning");
                toastr.error(resultResponse.Message, 'Error !');
            }
            $("#Modal_" + hdnImageName).modal('hide');
            CloseBlockPage();
        },
        error: function (e) {
            $("#solid-alerts .alert").addClass("bg-danger");
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(e.status + "," + e.statusText);
            CloseBlockPage();
        }
    });
}

function SaveMultipleCropedImage(saveFilePath, hdnImageName, postUrl) {
    //alert("Hidden field name" + hdnImageName);
    StartBlockPage();
    var $image = $("#cropimage_" + hdnImageName);
    var result = $image.cropper("getCroppedCanvas");
    var imgData = result.toDataURL('image/png');
    var extension = $("#fileImage_" + hdnImageName).val().split('.').pop();

    var model = {
        imgBase64String: imgData,
        saveFilePath: saveFilePath,
        extension: extension
    };

    $.ajax({
        type: 'POST',
        url: postUrl,
        data: model,
        //contentType: 'application/json; charset=utf-8',
        //dataType: 'JSON',
        success: function (resultResponse) {
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(resultResponse.Message);
            if (resultResponse.Status == 200) {
                console.log("1");
                $("#solid-alerts .alert").addClass("bg-primary");

                var div = '<div class="col-lg-3 col-md-6 col-xs-12 margintop20">';
                div += '<img class="img-thumbnail img-fluid img-custom-size" data-new="1" data-imagename="' + resultResponse.Result + '" src="' + imgData + '"/><br/><br/>';
                div += '<input type="radio" id="DefaultImage" name="DefaultImage" value="' + resultResponse.Result + '">Default</input><br/><br/>';
                div += '<a href="javascript:;" class="btn btn-danger removeImage">Remove</a>';
                div += '</div>';


                ModelImages.push(resultResponse.Result);
                console.log(ModelImages);
                $('#divMultiUpload').append(div);
                addImageName(resultResponse.Result);
                $('#spanErrMultiUpload').hide();

                $('#divMultiUpload .removeImage').on('click', function () {
                    var name = $(this).parent().find('img').data('imagename');
                    alert(name);
                    console.log(removeUrl);
                    $.ajax({
                        type: 'POST',
                        url: removeUrl + '?imagename=' + name,
                        //data: { imagename: name },
                        contentType: 'application/json; charset=utf-8',
                        dataType: 'JSON',
                        success: function (resultResponse) {                            
                            if (resultResponse.Status == 200) {
                                console.log("1");
                                toastr.success(resultResponse.Message, 'Success !');
                            }
                            else {
                                toastr.error(resultResponse.Message, 'Error !');
                            }
                        },
                        error: function (e) {
                            $("#solid-alerts").show();
                            $('#solid-alerts #message').text(e.status + "," + e.statusText);
                            CloseBlockPage();
                        }
                    });
                    $(this).parent().remove();
                });
            }
            else {
                $("#solid-alerts .alert").addClass("bg-warning");
                toastr.error(resultResponse.Message, 'Error !');
            }
            $("#Modal_" + hdnImageName).modal('hide');
            CloseBlockPage();
        },
        error: function (e) {
            $("#solid-alerts .alert").addClass("bg-danger");
            $("#solid-alerts").show();
            $('#solid-alerts #message').text(e.status + "," + e.statusText);
            CloseBlockPage();
        }
    });
}

function addImageName(imagename) {
    var imagename_array = imagename.split(',');
    for (var i = 0; i < imagename_array.length; i++) {
        var hdnImage = $('#HiddenModelImages').val();
        if (hdnImage != '') {
            hdnImage = hdnImage + ",";
        }
        hdnImage = hdnImage + imagename_array[i];
        console.log(hdnImage);
        $('#HiddenModelImages').val(hdnImage);
    }
}



//function removeImage() {
//    console.log('aaaaa');
//    $(document).on('click', '#divMultiUpload .removeimage', function () {
//        var name = $(this).parent().find('img').data('imagename');
//        alert(name);
//        setTimeout(function () {
//            $('.popup-tabs li a').removeClass('active');
//            $('.popup-tabs li:first-child a').addClass('active');
//            $("#upload").show();
//            $("#crop").hide();
//            $("#description").hide();
//        }, 100);
//    });


//    $.ajax({
//        type: 'POST',
//        url: '@Url.Action("RemoveImage","Product")',
//        data: { imagename: imagename },
//        contentType: 'application/json; charset=utf-8',
//        dataType: 'JSON',
//        success: function (resultResponse) {
//            console.log(resultResponse);
//            if (resultResponse.Status == 'Success') {
//                console.log("1");
//            }
//            else {
//                toastr.error(resultResponse.Message, 'Error !');
//            }
//        },
//        error: function (e) {
//            $("#solid-alerts").show();
//            $('#solid-alerts #message').text(e.status + "," + e.statusText);
//            CloseBlockPage();
//        }
//    });
//    $(this).parent().remove();



//}



