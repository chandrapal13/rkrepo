﻿using RkCore.Helper;
using RkCore.Model;
using RkServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static RkCore.Helper.Enums;

namespace RkAdmin.Controllers
{
    public class ProductController : Controller
    {
        private ProductServices _service;

        public ProductController()
        {
            _service = new ProductServices();
        }
        // GET: Product
        public ActionResult Index()
        {
            IQueryable<ViewProductModel> model = _service.GetProductBaseList();
            return View(model);
        }

        public ActionResult AddProduct()
        {
            AddProductModeltems model = new AddProductModeltems();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> AddProduct(AddProductModeltems model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrWhiteSpace(model.HiddenModelImages))
                {
                    model.ModelImages = model.HiddenModelImages.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.ToString()).ToArray();
                    mResult = await _service.AddEditProduct(model);
                    if(mResult.Status==ResponseStatus.Success)
                    {
                        TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.error, mResult.Message);
                        return RedirectToAction("Index");
                    }
                    //var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                    //ffMpeg.GetVideoThumbnail(GlobalConfig.PostImagePath + model.HiddenPostFileName, GlobalConfig.PostImagePath + shortGuid + ".jpg");
                }
            }
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> EditProduct(int id)
        {
            AddProductModeltems model = await _service.GetProductDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("AddProduct", model);
        }

        public async Task<JsonResult> DeleteProduct(int id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();           
            if (id > 0)
            {
                mResult = await _service.DeleteProduct(id);
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> RemoveImage(string imagename)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if(!string.IsNullOrEmpty(imagename))
            {
                mResult = await _service.DeleteFileName(imagename);
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}