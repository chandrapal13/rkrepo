﻿using RkCore.Model;
using RkServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static RkCore.Helper.Enums;

namespace RkAdmin.Controllers
{
    public class HomeController : Controller
    {
        private GeneralServices _service;

        public HomeController()
        {
            _service = new GeneralServices();
        }
        public ActionResult Index()
        {
            AdminDashboardModel model = _service.GetDeshboardInfo();
            return View(model);
        }

        public ActionResult Crop()
        {
            return View();
        }

        public async Task<ActionResult> Slider()
        {
            ViewBag.action = "Add";
            List<AddUpdateSliderModel> model =await _service.getSlider();
            return View(model);
        }

        public async Task<ActionResult> Add()
        {
            return View("AddOrUpdate");
        }

        [HttpPost]
        public async Task<ActionResult> Add(AddUpdateSliderModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if(ModelState.IsValid)
            {
                mResult = await _service.AddOrUpdateSlider(model);
                if (mResult.Status == ResponseStatus.Success)
                {
                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.error, mResult.Message);
                    return RedirectToAction("Slider");
                }
                else
                    return View("AddOrUpdate", model);
            }
            else
                return View("AddOrUpdate", model);

        }

        public async Task<ActionResult> DeleteSlider(int id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.DeleteSlider(id);
            if(mResult.Status==ResponseStatus.Success)
            {
                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.error, mResult.Message);
            }
            return Json(mResult.Status, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public async Task<ActionResult> ContactUs()
        {            
            List<ViewContactUsModel> model = await _service.GetContactUs();
            return View(model);
        }
         
       
    }
}