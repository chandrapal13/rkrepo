﻿using RkCore.Helper;
using RkCore.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RkAdmin.Controllers
{
    public class UtilityController : Controller
    {
        // GET: Utility
        [HttpPost]
        public JsonResult UploadCropImage(SaveImageModel model)
        {
            string imageName = ShortGuid.NewGuid().ToString() + "." + model.extension;
            string filePath = Path.Combine(model.saveFilePath, imageName);
            ResponseModel<object> mResult = Utilities.SaveCropImage(model.imgBase64String, filePath, model.extension, 715, 1070);
            mResult.Result = imageName;

            if (!string.IsNullOrEmpty(model.previousImageName))
            {
                string ExistFilePath = Path.Combine(model.saveFilePath, model.previousImageName);
                if (System.IO.File.Exists(ExistFilePath))
                {
                    System.IO.File.Delete(ExistFilePath);
                }
            }

            return Json(mResult);
        }

        public JsonResult UploadSliderCropImage(SaveImageModel model)
        {
            string imageName = ShortGuid.NewGuid().ToString() + "." + model.extension;
            string filePath = Path.Combine(model.saveFilePath, imageName);
            ResponseModel<object> mResult = Utilities.SaveCropImage(model.imgBase64String, filePath, model.extension, 823, 1920);
            mResult.Result = imageName;

            if (!string.IsNullOrEmpty(model.previousImageName))
            {
                string ExistFilePath = Path.Combine(model.saveFilePath, model.previousImageName);
                if (System.IO.File.Exists(ExistFilePath))
                {
                    System.IO.File.Delete(ExistFilePath);
                }
            }

            return Json(mResult);
        }
    }
}